#!/bin/bash
set -e
set -o errexit

ARCHIVE_MACHINE_PORT=${ARCHIVE_MACHINE_PORT:-22}

rsync_image() {
  rsync -e "ssh -p ${ARCHIVE_MACHINE_PORT} -i /tmp/archive_machine_user_key -o 'CheckHostIP no' -o 'UserKnownHostsFile /tmp/archive_machine_host_key'" $@
}
image_ssh() {
  ssh -p ${ARCHIVE_MACHINE_PORT} -i /tmp/archive_machine_user_key -o 'CheckHostIP no' -o 'UserKnownHostsFile /tmp/archive_machine_host_key' "${ARCHIVE_MACHINE_USER}@${ARCHIVE_MACHINE_HOST}" $@
}

#trap err_cleanup ERR
#trap err_cleanup EXIT

# For local builds via gitlab-runner exec, provide a mechanism to copy artifacts
# Use gitlab-runner exec docker --env PREVIOUS_STAGE_ARTIFACTS=/tmp/output --docker-volume
if [ ! -z ${PREVIOUS_STAGE_ARTIFACTS+x} ]; then
  echo "Manual mode, copying artifacts"
  cp -r "${PREVIOUS_STAGE_ARTIFACTS}" "${CI_PROJECT_DIR}/output"
  ls -la "${CI_PROJECT_DIR}/output"
fi

#ls -la ..
# Uncomment this for debugging - note that this will reveal your private keys!
# (DO NOT use in a public repo)
#env

echo "${ARCHIVE_MACHINE_USER_KEY}" >> /tmp/archive_machine_user_key
if [ "${ARCHIVE_MACHINE_PORT}" -eq "22" ]; then
  echo "${ARCHIVE_MACHINE_HOST} ${ARCHIVE_MACHINE_HOST_KEY}" >> /tmp/archive_machine_host_key
else
  echo "[${ARCHIVE_MACHINE_HOST}]:${ARCHIVE_MACHINE_PORT} ${ARCHIVE_MACHINE_HOST_KEY}" >> /tmp/archive_machine_host_key
fi

chmod 0600 /tmp/archive_machine_user_key
chmod 0600 /tmp/archive_machine_host_key

export ARCHIVE_DEST_DIR="${ARCHIVE_DIR}/${CI_COMMIT_REF_NAME}/${CI_PIPELINE_ID}"

image_ssh "mkdir -p ${ARCHIVE_DEST_DIR}"

rsync_image --exclude "repo/db" --recursive --progress "${CI_PROJECT_DIR}/output/repo" "${ARCHIVE_MACHINE_USER}@${ARCHIVE_MACHINE_HOST}:${ARCHIVE_DEST_DIR}"

echo "These artifacts can now be downloaded from: "
echo "${DOWNLOAD_SITE_BASE}${CI_COMMIT_REF_NAME}/${CI_PIPELINE_ID}"
