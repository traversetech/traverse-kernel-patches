#!/bin/sh
# For a CI environment, set global config

PATCHSET_VERSION=$(git rev-parse --short HEAD)


if [ ! -z "${CI+x}" ]; then
        echo "In CI environment, setting git user and email"
        git config --global user.email "build@example.com"
        git config --global user.name "Continuous Integration build"
        echo "Setting LOCALVERSION to pipeline IID, +${CI_PIPELINE_IID}"
        PATCHSET_VERSION="+${CI_PIPELINE_IID}"
fi
mkdir -p "output/kernel"

CHANGELOG_DIST=${CHANGELOG_DIST:-traverse-stretch}
SANITIZED_CODENAME=$(echo "${CHANGELOG_DIST}" | sed "s/_/-/g")

GIT_ARGS="--depth 1" ./download-apply.sh

KERNEL_EXTRAVERSION=$(sed -n -E 's/EXTRAVERSION = (.+)/\1/p' linux/Makefile)
OUR_EXTRAVERSION="${KERNEL_EXTRAVERSION}-traverse-${SANITIZED_CODENAME}"

UPLOADER_EMAIL=$(git log -1 --pretty=format:'%ae')
UPLOADER_FULLNAME=$(git log -1 --pretty=format:'%an')

time make -C linux LOCALVERSION="${PATCHSET_VERSION}" DEBEMAIL="${UPLOADER_EMAIL}" DEBFULLNAME="${UPLOADER_FULLNAME}" KDEB_SOURCENAME="linux-traverse" KDEB_CHANGELOG_DIST="${SANITIZED_CODENAME}" EXTRAVERSION="${OUR_EXTRAVERSION}" -j$(nproc) deb-pkg
rm -rf debs && mkdir debs
mv linux-*.deb debs
mv linux-*.tar* debs
mv linux-*.dsc* debs
mv linux-*.changes debs

mv *.diff.gz debs || :
mv *.buildinfo debs || :

cp -r debs/* "output/kernel"

du -h "output"
